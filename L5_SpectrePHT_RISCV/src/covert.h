
#ifndef COVERT_H
#define COVERT_H

#include <stdint.h>
#include <io.h>
#include "utils.h"
#include "sim.h"

#define BYTES_PER_LINE 64
#define LOG2_BYTES_PER_LINE 6 // keep in sync with BYTES_PER_LINE

#define LINES_PER_SET 16
#define SETS_COUNT 4
#define SET_OFFSET (BYTES_PER_LINE * LINES_PER_SET)
#define CACHE_SIZE (SETS_COUNT * SET_OFFSET)


// fit exactly L1D cache
// align to CACHE_SIZE
u8 shared_mem[CACHE_SIZE] __attribute__ ((aligned (CACHE_SIZE)));
u8 canary[BYTES_PER_LINE] __attribute__ ((aligned (SET_OFFSET))) ;
u8 evict_mem[CACHE_SIZE] __attribute__ ((aligned (CACHE_SIZE)));

void gadget(u8* secret_address, int bit_select, u8* shared_mem, u8 train);

static inline int permutation(u8 a, u8 mod) {
    return (a * 5 + 3) % mod;
}


#endif