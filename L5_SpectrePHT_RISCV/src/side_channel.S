#include <riscv_asm.h>


#define secret_ptr a0
#define bit_select a1
#define shared_mem_ptr a2
#define training_if_one a3

.global gadget
gadget:
  addi sp, sp,-8

  # ********** TODO ***************

  # Windowing gadget

  # Triggering speculation

  # Access secret

  # Select one bit of secret

  # Send bit



.global gadget_end
gadget_end:
  addi sp, sp, 8
  ret

