

# L5 SpectrePHT : la sécurité de la spéculation de direction de branchement

Dans cette leçon, nous allons utiliser SpectrePHT pour lire une donnée secrète, puis l'exfiltrer à l'aide du canal caché que vous avez développé dans la leçon 4.

## Énoncé

L’objectif ici est de lire le secret présent à l’adresse *SUPERVISOR_SECRET_ADDRESS* à l’aide de l’attaque [Spectre PHT](https://spectreattack.com/spectre.pdf).

Ce TP est complété en réalisant 2 fonctions :
- *spectre* : cette fonction est chargée d’ordonnancer les appels à votre *gadget* spectre pour exfiltrer le secret en entier. Dans une première phase, on entraîne le prédicteur de branchement **sans accéder au secret**.
    Puis, on lance l’attaque en exécutant un *load* normalement non autorisé sur le secret, suivi de la fonction *send* de votre canal caché. Enfin, on lit la donnée exfiltrée avec *receive*.
    Ce sont 2 appels au même *gadget* avec des arguments différents.
    Il y a donc une variable, qui prend deux valeurs différentes, s’il s’agit de l’entraînement ou de l’attaque.
- *gadget* dans le fichier *side_channel.S*. Il s’agit ici d’écrire la fonction en assembleur RISC-V, vous pourrez trouver un [petit guide ici](https://web.eecs.utk.edu/~smarz1/courses/ece356/notes/assembly/).
    Ce gadget doit :
    * Générer une fenêtre de spéculation
    * Déclencher la spéculation via un branchement
    * En mode spéculatif, lire une donnée autorisée pendant l’entrainement ou le secret interdit pendant l’attaque.
    * puis l’exfiltrer (*send*), toujours en mode spéculatif.

En mode d’entraînement, on lit une donnée autorisée et non un secret.