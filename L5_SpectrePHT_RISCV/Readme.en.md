# L5 SpectrePHT: The Security of Branch Prediction Speculation

In this lesson, we will use SpectrePHT to read a secret data, and then exfiltrate it using the covert channel you developed in lesson 4.

## Objectives

The objective here is to read the secret present at the address *SUPERVISOR_SECRET_ADDRESS* using the [Spectre PHT](https://spectreattack.com/spectre.pdf) attack.

This practical session is completed by implementing 2 functions:
- *spectre*: This function is responsible for orchestrating calls to your Spectre *gadget* to fully exfiltrate the secret. In the first phase, we train the branch predictor **without accessing the secret**.
    Then, we launch the attack by executing a normally unauthorized *load* on the secret, followed by the *send* function of your covert channel. Finally, we read the exfiltrated data with *receive*.
    These are 2 calls to the same *gadget* with different arguments.
    There is therefore a variable, which takes two different values, whether it is for training or the attack.
- *gadget* in the *side_channel.S* file. This involves writing the function in RISC-V assembly, for which you can find a [brief guide here](https://web.eecs.utk.edu/~smarz1/courses/ece356/notes/assembly/).
    This gadget must:
    * Generate a speculation window
    * Trigger speculation via a branch
    * In speculative mode, read an authorized data during training or the forbidden secret during the attack.
    * then exfiltrate it (*send*), still in speculative mode.

In training mode, an authorized data is read, not a secret.