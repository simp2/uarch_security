# L2 : Les Caches CPU

## Description de votre cible

Commencez par identifier **précisemment** votre processeur et recherchez des ressources sur sa microarchitecture (taille des caches, unités d'exécutions, etc.). 
Quelques ressources utiles :
- Le site [Anandtech](https://www.anandtech.com/) fait des analyses détaillées des microarchitectures.
- Le site [Chips and cheese](https://chipsandcheese.com/) est un site communautaire sur le fonctionnement des microarchitectures.

## Objectif
Dans cet exercice, votre objectif est de développer un histogramme visualisant les différents temps d'accès des caches lors des hits et des misses.

## Description
Pour résoudre l'exercice, vous devez implémenter les deux TODOs à l'intérieur de `cache_measure.c`.

## Compilation et Exécution
Vous pouvez construire le code en exécutant la commande `make`. Cela compilera le fichier `cache_measure.c`.
Ensuite, vous pouvez exécuter le fichier et visualiser les résultats de la manière suivante :
```
./cache_measure
./plot.sh
```
