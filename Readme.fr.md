# La sécurité de la microarchitecture

Ce TP explore les vulnérabilités des cœurs à exécution dans le désordre (out-of-order) dues à l'exécution spéculative.

Nous expérimenterons sur deux cibles différentes : d’une part votre propre PC avec le jeux d’instruction x86, de l’autre un cœur (le NaxRISCV) simulé.
La simulation du Nax nous permettra d’observer finement le comportement du cœur, ce qui n’est pas possible sur votre machine.

Le TP se décompose en leçons :
- L1: observation du comportement in-order et out-of-order.
- L2: observer les différences de timings lors d’accès au cache sur votre machine.
- L3: utiliser ces différences pour monter un canal de communication caché.
- L4: créér un canal de communication caché sur le Nax (pas forcémment à l’aide des caches).
- L5: monter une attaque SpectrePHT sur le Nax.
- L6: monter une attaque SpectrePHT sur votre machine.
- L7: attaque SpectreBTB sur le Nax.
- L8: attaque SpectreRSB sur le Nax.


## Fonctionnement du TP

### Compte rendu

Ce TP fait l'objet d'un compte rendu expliquant, pour chaque leçon, votre démarche et vos résultats.
Pour chaque leçon, il vous faudra décrire *en détail* le fonctionnement de l'attaque.
Une attention particulière doit être portée aux subtilités à introduire dans le code pour que cela fonctionne.
N'hésitez pas à mettre quelques mots sur les fausses pistes et pourquoi elles n'ont pas marché.

**Chaque question d'un énoncé doit être répondue explicitement.**

### Nix

Ce TP repose sur le gestionnaire de paquets Nix pour établir un environnement de travail reproductible.
Pour son fonctionnement, il faut donc avoir Nix installé.

```bash
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
```

Lors de la première exécution d’un script utilisant un shell Nix, les dépendances nécessaire vont être installées.
Il peut se passer un certain temps, où le terminal n’affiche rien. **Ce n’est pas un bug**. Il suffit d’être patient.


## x86

*Cette partie du TP est adapté de TPs proposés par Daniel Weber et Michael Schwarz. Ils sont les principaux auteurs des TPs x86. TPs utilisés ici avec leur accord.*

Vous aurez besoin d'un PC avec un processeur x86, utilisant Linux **nativement** : les VMs risquent de ne pas fonctionner.
Si vous n'avez pas de Linux natif, associez-vous avec un autre étudiant pour ces leçons.

Ces TPs ciblent surtout les processeurs Intel, mais peuvent fonctionner avec d'autres.
Suivant le processeur, ces TPs marcheront plus ou moins bien. Il vous faudra comprendre pourquoi **votre** processeur est vulnérable ou immunisé.

### Réduction du bruit

Les attaques microarchitecturales sont sensibles au bruit des opérations concurrentes sur le PC.
Pour de meilleurs résultats :
- branchez votre PC au secteur.
- Arrêtez les programmes non essentiels, en particulier s'ils impliquent vidéo ou musique.

## NaxRISCV


### Synthétiser le cœur (à faire au moins une fois)

Pour le simuler, il faut d'abord synthétiser le cœur à partir de ses sources.
Le cœur Nax est développé en SpinalHDL.
La synthèse ici désigne la génération du Verilog décrivant le cœur, puis la génération d'un exécutable C++ simulant ce cœur avec Verilator.

Pour vous, il suffit d'appeler
```bash
./nax/build-core.sh
```

### Compiler un binaire

Pour chaque leçon, vous pouvez compiler votre binaire à l'aide de make.
Utilisez le script *make.sh* pour encapsuler la commande dans l'environnement Nix.

```
./make.sh
# ou
./make.sh clean
```

### Lancer une simulation

Une simulation consiste à charger un fichier **elf** sur le cœur simulé puis à l'exécuter. Nous avons le choix sur les sorties générées par cette simulation :
- **simulate** : ne fait que simuler le cœur sans générer de traces (le plus rapide)
- **trace** : simule et génère une trace Gem5 (trace de l'état du pipeline), visualisable à l'aide de Konata.
- **waveform** : simule et génère une trace VCD (états de l'ensemble des signaux du processeur au cours de son exécution) visualisable à l'aide de GTKWave.

Cette commande est encapsulée dans le script *execute.sh*.

```bash
./execute.sh
```

À l'intérieur du script, que vous pouvez modifier si besoin, vous devez donner en argument le binaire à charger à l'une des 3 commandes ci-dessus.

Par exemple
```bash
trace "L1-Pathfinding.elf"
```

### Visualiser les traces

Les traces Gem5 peuvent être visualisées à l'aide de **Konata**, par exemple en exécutant la commande

```bash
./view_trace.sh
```
dans les différentes leçons.

Les waveforms peuvent être lues à l'aide du logiciel libre gtkwave, mais ce n'est pas nécessaire dans ce TP.