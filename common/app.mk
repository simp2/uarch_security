THIS_FILE := $(realpath $(lastword $(MAKEFILE_LIST)))
THIS_DIR := $(dir $(THIS_FILE))
THIS_DIR := $(patsubst %/,%,$(THIS_DIR))

LDSCRIPT ?= $(THIS_DIR)/app2.ld
CFLAGS += -DUSE_GP
include $(THIS_DIR)/asm.mk
