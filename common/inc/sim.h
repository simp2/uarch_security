#pragma once
#ifndef SIMH
#define SIMH

#include "type.h"
#include "io.h"

#include "sim_asm.h"

static int sim_putchar(int c){
    write_u32(c, 0x10000000);
    return c;
}

static int sim_puts(char *c){
    while(*c){
        write_u32(*c++, 0x10000000);
    }
}

static long __attribute__ ((noinline)) sim_time(){
    return read_u32(CLINT_TIME);
}

static void sim_puthex(u32 c){
    write_u32(c, 0x10000008);
}

void global_sim_puthex(u32 c);

static void sim_putudec(u32 c){
    write_u32(c, 0x10000068);
}


static void sim_putdec(u32 c){
    write_u32(c, 0x10000060);
}

#endif