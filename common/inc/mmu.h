#include "type.h"
#include "riscv.h"

#define mmu_tables_0 0x90000000l
#define mmu_tables_1 0x90001000l
#define mmu_tables_2 0x90002000l

void mmu_set_tree(u64 table,u64 level,u64 virt,u64 next) {
  u64* addr = (u64*) (table + ((virt >> (level*9+9)) & 0xFF8));
  u64 entry = ((u64)next >> 2) | 0x01;
  *addr = entry;
}

void mmu_set_leaf(u64 table,u64 level,u64 virt, u64 phys, u64  flags) {
  u64* addr = (u64*) (table + ((virt >> (level*9+9)) & 0xFF8));
  u64 entry = (phys >> 2) | flags;
  *addr = entry;
}


void mmu_disable() {
  #if __riscv_xlen == 64
  csr_write(satp, 0);
  #endif
}

void mmu_enable(u64 table) {

  #if __riscv_xlen == 64
  // Sv39
  csr_write(satp, (table >> 12) | (8l << 60));
  asm("sfence.vma");
  #endif
}


void mmu_init() {
    //Map program into user space
    mmu_set_leaf(mmu_tables_1, 1, 0x80000000, 0x80000000, 0xDF);


    mmu_set_tree(mmu_tables_0, 2, 0x80000000, mmu_tables_1);

    //Map IO into user space
    mmu_set_leaf(mmu_tables_2, 1, 0x10000000, 0x10000000, 0xDF);
    mmu_set_tree(mmu_tables_0, 2, 0x10000000, mmu_tables_2);

    mmu_enable(mmu_tables_0);
}