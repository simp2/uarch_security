# NaxRISCV

You can find a presentation of the core in [NaxPresentation.md](./NaxPresentation.md).

## Get all the sources

Fetch all submodules :
```bash
git submodule update --init --recursive
```

## Build the Spike simulator

Launch the following script. It uses a nix shell, so you need to have [nix installed](https://nixos.org/download.html).
```shell
./build-isa-sim.sh
```

## Launch Nix

In the root folder, open a terminal and launch the nix shell ([need to have Nix installed if not already there](https://nixos.org/download.html)).

```bash
nix-shell env/shell.nix --pure
```

## Build the core

Build the Nax core
```bash
sbt "runMain naxriscv.Gen64"
```

## Compile the simulator

```bash
cd $NAXRISCV/src/test/cpp/naxriscv
make compile
```

## Lanch the tests

```shell
cd $NAXRISCV/src/test/cpp/naxriscv
./testsGen.py
make clean compile
make test-clean && make test-all -j$(nproc); make test-report
```