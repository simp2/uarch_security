#!/usr/bin/env nix-shell
#!nix-shell ../env/shell.nix -i bash --pure

if [ "$1" != "nosim" ]; then
    # build simulator
    cd $NAXRISCV/ext/riscv-isa-sim
    mkdir build
    cd build
    ../configure --prefix=$RISCV --enable-commitlog  --without-boost --without-boost-asio --without-boost-regex
    make -j$(nproc)
    g++ --shared -L. -Wl,--export-dynamic -Wl,-rpath,/lib  -o package.so spike.o  libspike_main.a  libriscv.a  libdisasm.a  libsoftfloat.a  libfesvr.a  libfdt.a -lpthread -ldl -lboost_regex -lboost_system -lpthread  -lboost_system -lboost_regex
fi

# build core

## generate verilog
cd $NAXRISCV
sbt "runMain naxriscv.Gen64"

## compile verilog into an executable with verilator
cd $NAXRISCV/src/test/cpp/naxriscv
make clean compile
mkdir -p $NAXRISCV/bin
cp ./obj_dir/VNaxRiscv $NAXRISCV/bin/naxriscv