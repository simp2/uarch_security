# L3 : Canaux cachés

*Adapté d’un TP conçu par Daniel Weber et Michael Schwarz.*

## Objectif
Dans cet exercice, votre objectif est de développer deux processus, un émetteur et un récepteur, qui communiqueront via le cache CPU.

## Description
Pour résoudre l'exercice, vous devez implémenter les TODOs à l'intérieur de `sender.c` et `receiver.c`.

## Compilation et Exécution
Vous pouvez construire le code en exécutant la commande `make`. Cela compilera les fichiers `sender.c` et `receiver.c`.
Ensuite, vous devez ouvrir deux fenêtres de terminal.
D'abord, vous démarrez un terminal avec le processus émetteur :
```
./sender
```
Ensuite, vous démarrez un terminal avec le processus récepteur :

```
./receiver
```
Après cela, vous pouvez commencer à taper des messages dans l'invite de l'émetteur qui seront reçus et affichés par le second processus.

Une solution fonctionnelle pourrait ressembler à ceci :
Terminal 1 :
```
./sender
Entrez le caractère à envoyer : C
Envoyé 'C'
Entrez le caractère à envoyer : O
Envoyé 'O'
Entrez le caractère à envoyer : V
Envoyé 'V'
Entrez le caractère à envoyer : E
Envoyé 'E'
Entrez le caractère à envoyer : R
Envoyé 'R'
Entrez le caractère à envoyer : T
Envoyé 'T'
Entrez le caractère à envoyer :
```

Terminal 2 :
```
./receiver
Reçu 'C'
Reçu 'O'
Reçu 'V'
Reçu 'E'
Reçu 'R'
Reçu 'T'
```