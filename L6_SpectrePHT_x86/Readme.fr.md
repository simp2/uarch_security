# L6 : Quand les prédictions tournent mal

## Objectif

Dans cet exercice, votre objectif est de mettre en œuvre une attaque Spectre-PHT (également connue sous le nom de Spectre V1).

## Description

Pour résoudre l'exercice, vous devez implémenter les TODOs à l'intérieur de `spectre.c`.

## Compilation et Exécution

Vous pouvez construire le code en exécutant la commande `make`. Cela compilera le fichier `spectre.c`.
Ensuite, vous pouvez simplement exécuter l'exploit :

```
./spectre
```

Si cela ne fonctionne pas immédiatement, soyez patient et donnez-lui quelques secondes ou redémarrez l'attaque.
Une attaque réussie pourrait ressembler à ceci :


```
./spectre
[+] Flush+Reload Threshold: 188
[+] Current Leak: C
[+] Current Leak: CP
[+] Current Leak: CPV
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5
[+] Current Leak: CPV5_4R3_H4
[+] Current Leak: CPV5_4R3_H4V
[+] Current Leak: CPV5_4R3_H4VNT3D!
```
