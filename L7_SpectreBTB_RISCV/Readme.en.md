# L5: Using Branch Target Buffer (BTB) Speculation

## Objective

Following the model of lesson 5, the goal is to retrieve the same secret, but by exploiting another type of speculation: jump destination speculation, based on the Branch Target Buffer (BTB).

You are again required to fill in the `spectre` function in the *main.c* file and the `gadget` in the *side_channel.S* file.

This gadget must:
- Generate a speculation window
- Trigger speculation via an indirect jump `jr`
- In speculative mode, read the secret...
- then exfiltrate it (*send*), still in speculative mode.

In training mode, the jump will use the exfiltration gadget on an authorized data.
In attack mode, the jump does not "architecturally" use the exfiltration gadget, but still fills the address variable with the address of the secret. It is the speculation that will trigger the exfiltration gadget.

---

This translation provides an overview of the task, which involves implementing speculation techniques in a controlled environment, using both training and attack modes to manipulate data handling and exfiltration processes.