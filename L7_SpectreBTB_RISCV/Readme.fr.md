# L5: Utiliser la spéculation du BTB

## Énoncé

Sur le modèle de la leçon 5, le but est de retrouvé le même secret, mais en exploitant un autre type de spéculation: la spéculation de destination de saut, basée sur le BTB.

Il vous faut là encore remplir les fonctions `spectre` dans le fichier *main.c* et le `gadget` dans *side_channel.S*.

Ce gadget doit :
    * Générér une fenêtre de spéculation
    * Déclencher la spéculation via un saut indirect `jr`
    * En mode spéculatif, lire le secret...
    * puis l’exfiltrer (*send*) toujours en mode spéculatif.

En mode d’entraiment, le saut va utiliser le gadget d’exfiltration sur une donnée autorisée.
En mode d’attaque, le saut n’utilise pas "architecturellement" le gadget d’exfiltration, mais rempli tout de même la variable d’adresse avec l’adresse du secret. C’est la spéculation qui va déclancher le gadget d’exfiltration.