
/*
The side channel attack used here is to let the CPU speculatively do the following
1) execute a load that we don't have access to (page fault)
2) read its data and mask the bit we are interrested in
3) execute a load that the address depend on that given bit value (which may trigger a cache line refill)
Once back into the none speculative regime, we can look at the side effect in the data cache, by looking of the given line is loaded or not,
telling use the value of the bit we were interested into.
To work, you need to enable sideChannels = true in the Config.plugins, which will let the data cache write load values to the register file even when mmu checks failed
compile via :
make rv64 ima
Run in sim via :
obj_dir/VNaxRiscv --name play --load-elf ../../../../ext/NaxSoftware/baremetal/side_channel/build/rv64ima/side_channel.elf --start-symbol _start --pass-symbol pass --fail-symbol fail --trace --trace-ref --stats-print-all --trace --trace-ref --spike-debug
*/

#include <type.h>
#include <sim.h>
#include <stdlib.h>
#include "privilege.h"
#include "utils.h"
#include "mmu.h"
#include "io.h"
#include "side_channel.h"
#include "string.h"
#include "covert.h"

//We need to know the number of way to "invalidate" the data cache, allowing a easy line refill timing mesurement
#define DATA_CACHE_WAYS 4
#define KERNEL_USER 0 // 1 if secret data is in kernel memory and sidechannel == true

//Many address are defined here, notice that they are arrange in a way to avoid cache set and mmu set conflicts
#define USER_STACK 0x80010000l
#define SECRET_OFFSET 0x20100
#define SECRET_PHYSICAL (0x80000000 | SECRET_OFFSET)

#define SUPERVISOR_SECRET_ADDRESS  (0xB0800000 | SECRET_OFFSET)
#define USER_SECRET_ADDRESS  (0xB0C00000 | SECRET_OFFSET)

#define SECRET "Secret\n"
#define SECRET_LENGTH 8
volatile char *secret = SECRET;
volatile char decoded[SECRET_LENGTH+1];


void supervisor_main();
void user_test_page_fault();
void spectre();

void fail();
void assert(int conditione){
  if(!conditione) {
    sim_puts("FAIL\n");
    fail();
  }
}

int main(){
    sim_puts("*** main enter ***\n");

    mmu_set_leaf(mmu_tables_1, 1, SUPERVISOR_SECRET_ADDRESS, 0x80000000, 0xDF); //Provide supervisor access to the secret
    mmu_init();

    strcpy((volatile char *)SECRET_PHYSICAL, secret);


    //Some warmup to load the caches and TLB with the attack code
    sim_puts("*** warmup ***\n");
    machine_to_user((u64)spectre, USER_STACK);
    assert(csr_read(mcause) == CAUSE_ECALL);
    decoded[SECRET_LENGTH] = 0;
    sim_puts(decoded);

    //Real attack which should print the secret by accessing SUPERVISOR_SECRET_ADDRESS
    sim_puts("*** attack ***\n");
    machine_to_user((u64)spectre, USER_STACK);
    assert(csr_read(mcause) == CAUSE_ECALL);
    decoded[SECRET_LENGTH] = 0;
    sim_puts(decoded);
    assert(strcmp(decoded, secret) == 0);

    sim_puts("*** main exit ***\n");
    return 0;
}


void user_test_page_fault(){
    sim_puts("*** user_test_page_fault ***\n");
    sim_putchar(*((volatile char*)SUPERVISOR_SECRET_ADDRESS));
}


void spectre(){
    volatile char* secret_address_reg = (volatile char*)SUPERVISOR_SECRET_ADDRESS;

    uint32_t total = 0;
    uint32_t correct = 0;

    sim_puts("*** spectre ***\n");

    for(int char_id = 0; char_id < SECRET_LENGTH;char_id++){
        u8 secret = secret_address_reg[char_id];

        decoded[char_id] = 0;
        for(int bit = 0; bit < 8; bit++) {

            u8 rx = 0;

            // ********** TODO ***************

            u8 real_bit = (*((u8*)((u64)secret_address_reg) + char_id) >> bit) & 1;
            if(rx == real_bit) {
                correct += 1;
                sim_puts("+");
            }
            else {
                sim_puts("-");
            }

            total += 1;
            decoded[char_id] |= rx << bit;
        }
    }

    sim_puts("\nSuccess rate : ");
    sim_putdec(correct);
    sim_puts("/");
    sim_putdec(total);
    sim_puts("\n");
} 