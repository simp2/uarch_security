#include <riscv.h>

void side_channel_attack(u64 ptr, u64 mask, u64 lines, u64 ptr_funct);
void spectre_gadget(void);
void skip_speculative(void);