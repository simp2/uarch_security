with (import <nixpkgs> {});

stdenv.mkDerivation {
    name = "riscv32-inria-elf";
    version = "0.1";

    src = fetchurl {
        url = "https://gitlab.inria.fr/rlasherm/riscv_gcc_variations/-/raw/multilib/riscv-medany-spec.tar.xz";
        hash =  "sha256-vB/XYNCDDdunPrsq/fV9YlO+jllEJDeG3QON47MsAGY=";
    };

    nativeBuildInputs = [autoPatchelfHook ];

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
        mkdir -p $out
        cp -r * $out
    '';
  
}

