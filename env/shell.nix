/*
 * File: naxriscv.nix
 * Project: env
 * Created Date: Monday November 14th 2022
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 14th November 2022 3:45:58 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2022 INRIA
 */

let
  pkgs = import (builtins.fetchGit {
    name = "pinned_nix_packages";
    url = "https://github.com/nixos/nixpkgs/";
    ref = "nixos-22.05";
    rev = "ce6aa13369b667ac2542593170993504932eb836";
  }) {};

  riscv-toolchain = import ./riscv-medany-multilib-toolchain.nix;
  konata = import ./konata.nix;
in

with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "naxriscv-build";

  SPINALHDL = toString ../nax/SpinalHDL;
  NAXRISCV  = toString ../nax;
  RISCV     = toString riscv-toolchain;
  RISCV_PATH = toString riscv-toolchain;
  SIM       = toString ../nax/bin/naxriscv;

  MEM_LATENCY = 10;
  TIMEOUT     = 1000000;

  nativeBuildInputs = with pkgs; [
    spike
    elfio
    verilator
    sbt
    zlib
    SDL2
    dtc
    boost
    python3
    python3Packages.matplotlib
    konata

    # needed by the gcc install
    isl
    libmpc
    riscv-toolchain
    qtrvsim
  ];

  commands = ''
    simulate () {
      $SIM --load-elf $1 --memory-latency $MEM_LATENCY --timeout $TIMEOUT --pass-symbol=pass --fail-symbol=fail
    }

    trace () {
      $SIM --load-elf $1 --memory-latency $MEM_LATENCY --timeout $TIMEOUT --pass-symbol=pass --fail-symbol=fail --trace-gem5
    }

    waveform () {
      $SIM --load-elf $1 --memory-latency $MEM_LATENCY --timeout $TIMEOUT --pass-symbol=pass --fail-symbol=fail --trace --trace-gem5
    }
    
    SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
  '';

  shellHook = ''
    eval "$commands"
    export PATH=$PATH:$RISCV/bin 
  '';
}
