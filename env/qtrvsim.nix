/*
 * File: naxriscv.nix
 * Project: env
 * Created Date: Monday November 14th 2022
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 14th November 2022 3:45:58 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2022 INRIA
 */

let
    pkgs = import (builtins.fetchGit {
        # Descriptive name to make the store path easier to identify                
        # name = "pinned_nix_packages";                                                 
        # url = "https://github.com/nixos/nixpkgs/";                       
        # ref = "nixos-22.05";                     
        # rev = "ce6aa13369b667ac2542593170993504932eb836";
        name = "pinned_nix_packages";                                                 
        url = "https://github.com/nixos/nixpkgs/";                       
        ref = "nixos-24.05";                     
        rev = "7e1ca67996afd8233d9033edd26e442836cc2ad6";                                   
    }) {};

    
    konata = (import ./konata.nix);
in

with import <nixpkgs> { 
#     crossSystem = {
#         config = "riscv32-unknown-linux-gnu";
#   };
};

# Make a new "derivation" that represents our shell
stdenv.mkDerivation {
    name = "naxriscv-build";

    SPINALHDL = toString ../nax/SpinalHDL;
    NAXRISCV = toString ../nax;
    # SIM = ../src/test/cpp/naxriscv/obj_dir/VNaxRiscv;
    RISCV = toString riscv-toolchain;
    RISCV_PATH = toString riscv-toolchain;
    SIM = toString ../nax/bin/naxriscv;

    MEM_LATENCY = 10;
    TIMEOUT = 1000000;

    # The packages in the `buildInputs` list will be added to the PATH in our shell
    nativeBuildInputs = with pkgs; [
        konata
        qtrvsim
    ];

    commands = ''  
    SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
    '';

    shellHook = ''
        eval "$commands"
        export PATH=$PATH:$RISCV/bin 
    '';

}
