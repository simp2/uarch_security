#!/usr/bin/env nix-shell
#!nix-shell ../env/shell.nix -i bash --pure

eval "$commands"
cd "$SCRIPTPATH"


# if arg1 is "clean" then clean

if [ "$1" = "clean" ]; then
    make clean
    echo "Cleaned"
    make
else
    make
fi