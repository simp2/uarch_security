#include <type.h>
#include <sim.h>
#include <stdlib.h>
#include <privilege.h>
#include <mmu.h>
#include <io.h>
#include <string.h>
#include "covert.h"

void zeroes() {
    // ********** TODO ***************    
}


void send(u8 b) {
    // ********** TODO ***************
}


u8 receive() {
    // ********** TODO ***************
    return 0;
}

int main(){
    sim_puts("*** start ***\n");
    
    // Initialiser la MMU
    mmu_init();

    // Définir le nombre de tests à effectuer
    int tests = 100;
    int success = 0;

    // Effectuer une série de tests d'envoi et de réception
    for(int i = 0; i < tests; i++) {

        // Générer une valeur à envoyer
        int val = ((i * 10) >> 2) % 2;

        // Effectuer une opération d'éviction avant l'envoi
        zeroes();

        // Insérer des opérations nop pour créer un délai, évitant que l'éviction ne touche spéculativement d'autres adresses
        REPEAT32(nop;);

        // Envoyer et recevoir la valeur
        send(val);
        u8 rx = receive();
        
        // Comparer la valeur reçue à la valeur envoyée
        if (rx == val) {
            success++;
            sim_puts("+");
        }
        else {
            sim_puts("-");
        }
    }

    // Afficher le taux de réussite
    sim_puts("\nSuccess rate: ");
    sim_putudec(success);
    sim_puts("/");
    sim_putudec(tests);
    sim_puts("\n");

    // Indiquer la fin du programme principal
    sim_puts("*** exit ***\n");
    pass();
    return 0;
}
