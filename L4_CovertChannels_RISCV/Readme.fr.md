# L4 Établissement d'un canal de communication caché

Dans cette leçon, nous allons établir un canal caché.
C'est à dire que nous allons créer deux fonctions *send* et *receive* permettant de communiquer 1 bit de données à l'aides d'états microarchitecturaux.

## Énoncé

La fonction *main* va tenter d'envoyer 100 fois 1 bit à l'aide de *send* et vérifier si *receive* reçoit la bonne valeur.
Le taux de succès est affiché. Pour réussir ce TP, vous devez avoir un taux le plus haut possible.
90% est le taux minimal pour avoir la moitié des points sur cette leçon.

- Choisir le support microarchitectural de votre canal caché*.
- Comment, avec quelles instructions, pouvez-vous initialiser votre canal à 0 ? Compléter la fonction *zeroes*.
- Comment envoyer un 1 ou un 0 ? Compléter la fonction *send*, n'acceptant que 0 ou 1 en entrée.
- Comment lire le bit envoyé ? Compléter la fonction *receive*.

- Valider la fonctionnalité de votre canal à l'aide de la trace Gem5 : faites des capture d'écran des états microarchitecturaux lors du *receive*.
- Évaluer votre taux de succès. Si insuffisant, réfléchissez à comment l'améliorer, en améliorant le canal ou à l'aide de correcteur d'erreurs.

- Établir le modèle de menace lié à ce canal caché :
    * La communication est-elle possible d'un processus utilisateur à un autre ?
    * La communication est-elle possible d'un niveau de privilège à un autre ?
    * Comment intéragit la mémoire virtuelle avec ce canal caché ?



*L’utilisation d’une mémoire partagée entre l’émetteur et le récepteur est autorisée, **mais il est impossible d’y écrire**: seule les instructions *load* sont autorisées sur cette mémoire partagée.