

# L4 Establishing a Covert Communication Channel

In this lesson, we will establish a covert channel.
That is, we will create two functions *send* and *receive* that can communicate 1 bit of data using microarchitectural states.

## Objective

The *main* function will attempt to send 1 bit 100 times using *send* and check if *receive* gets the right value.
The success rate is displayed. To succeed in this lab, you must have as high a rate as possible.
90% is the minimum rate to get half the points on this lesson.

- Choose the microarchitectural support for your covert channel*.
- How, with which instructions, can you initialize your channel to 0? Complete the *zeroes* function.
- How to send a 1 or a 0? Complete the *send* function, accepting only 0 or 1 as input.
- How to read the sent bit? Complete the *receive* function.

- Validate the functionality of your channel using the Gem5 trace: take screenshots of the microarchitectural states during *receive*.
- Assess your success rate. If insufficient, think about how to improve it, by improving the channel or using error correction.

- Establish the threat model related to this covert channel:
    * Is communication possible from one user process to another?
    * Is communication possible from one privilege level to another?
    * How does virtual memory interact with this covert channel?

*The use of memory shared between the sender and receiver is allowed, **but it is impossible to write to it**: only *load* instructions are allowed on this shared memory.

