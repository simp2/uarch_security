#ifndef SORTED_LIST_H
#define SORTED_LIST_H

#include <stdbool.h>
#include "pathfinding.h"


typedef struct SortedList {
    Node* nodes_ptr[MAP_SIZE*MAP_SIZE];
    uint8_t distances[MAP_SIZE*MAP_SIZE];
    uint32_t size;
} SortedList;

void SortedList_init(SortedList* list);
void SortedList_insert(SortedList* list, Node* node);
Node* SortedList_pop(SortedList* list);
bool SortedList_isempty(SortedList* list);
void display_list(SortedList* list);

#endif // SORTED_LIST_H