#include "sorted_list.h"
#include "pathfinding.h"
#include <stddef.h>
#include "sim.h"

#define MAX_LIST_SIZE (MAP_SIZE * MAP_SIZE)

void SortedList_init(SortedList* list) {
    list->size = 0;
    
    for(int i = 0; i < MAX_LIST_SIZE; i++) {
        list->distances[i] = 0xFF;
        list->nodes_ptr[i] = NULL;
    }
}

void SortedList_insert(SortedList* list, Node* node) {
    if(list->size >= MAX_LIST_SIZE) {
        return;
    }
    
    uint8_t fCost = node->gCost + node->hCost;
    
    uint8_t index_to_insert = 0;
    // dicotomic search for index_to_insert
    uint8_t min = 0;
    uint8_t max = list->size - 1;

    while(min < max) {
        uint8_t mid = (min + max) / 2;

        if(list->distances[mid] < fCost) {
            min = mid+1;
        }
        
        if(list->distances[mid] > fCost) {
            max = mid-1;
        }

        if(list->distances[mid] == fCost) {
            min = mid;
            max = mid;
        }
    }

    index_to_insert = min;
    
    // shift elements
    for(int j = list->size; j > index_to_insert; j--) {
        list->distances[j] = list->distances[j - 1];
        list->nodes_ptr[j] = list->nodes_ptr[j - 1];
    }
    
    list->distances[index_to_insert] = fCost;
    list->nodes_ptr[index_to_insert] = node;
    list->size++;
}

void display_list(SortedList* list) {
    sim_puts("List: ");
    for(int i = 0; i < list->size; i++) {
        sim_puts("(");
        sim_puthex(list->nodes_ptr[i]->position.x);
        sim_puts(",");
        sim_puthex(list->nodes_ptr[i]->position.y);
        sim_puts(")->");

        sim_puthex(list->distances[i]);
        sim_puts(" ");
    }
    sim_puts("\n");
}

Node* SortedList_pop(SortedList* list) {
    if(list->size == 0) {
        return NULL;
    }
    
    Node* node = list->nodes_ptr[0];
    
    // shift elements
    for(int i = 0; i < list->size - 1; i++) {
        list->distances[i] = list->distances[i + 1];
        list->nodes_ptr[i] = list->nodes_ptr[i + 1];
    }

    list->distances[list->size - 1] = 0xFF;
    list->nodes_ptr[list->size - 1] = NULL;
    
    list->size--;
    
    return node;
}

bool SortedList_isempty(SortedList* list) {
    return list->size == 0;
}