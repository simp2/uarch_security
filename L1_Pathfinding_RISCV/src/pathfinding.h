
#ifndef PATHFINDING_H
#define PATHFINDING_H

#include <stdint.h>

#define MAP_SIZE 8

typedef struct Vector2D
{
    uint8_t x;
    uint8_t y;
} Vector2D;

typedef struct Node
{
    Vector2D position;
    uint8_t gCost; // Movement cost (since start)
    uint8_t hCost; // Heuristic cost (estimated cost to reach end)
    struct Node* parent;
} Node;

#endif