#include <type.h>
#include <stdlib.h>
#include <privilege.h>
#include <mmu.h>
#include <io.h>
#include <string.h>
#include "pathfinding.h"
#include "sorted_list.h"
#include "sim.h"



#define MAP_OK 0
#define MAP_BLOCKED 1
#define MAP_PATH 2
#define MAP_START 3
#define MAP_END 4

void fail();
void pass();
void assert(int condition){
    if(!condition) {
        sim_puts("FAIL\n");
        fail();
    }
}


// Map generation
void map_init(uint8_t map[MAP_SIZE][MAP_SIZE]) {
    for(int i = 0; i < MAP_SIZE; i++) {
        for(int j = 0; j < MAP_SIZE; j++){
            map[i][j] = MAP_OK;
        }
    }

    for(int i = 1; i < 8; i++) {
        map[1][i] = MAP_BLOCKED;
    }

    for(int i = 3; i < 8; i++) {
        map[i][4] = MAP_BLOCKED;
    }
    map[3][3] = MAP_BLOCKED;
}

// To pretty print the map
void display_map(uint8_t map[MAP_SIZE][MAP_SIZE]) {

    char buffer[8];
    for(int i = 0; i < MAP_SIZE; i++) {
        sim_puts("|");
        for(int j = 0; j < MAP_SIZE; j++){
            sim_puts(" ");
            switch (map[i][j]) {
                case MAP_OK:
                    sim_puts(" ");
                    break;
                case MAP_BLOCKED:
                    sim_puts("x");
                    break;
                case MAP_PATH:
                    sim_puts(".");
                    break;
                case MAP_START:
                    sim_puts("s");
                    break;
                case MAP_END:
                    sim_puts("e");
                    break;
                default:
                    sim_puts("?");
                    break;
            }
            // sim_puthex( map[i][j]);
            sim_puts(" |");
        }
        sim_puts("\n");
    }
    sim_puts("\n");
}

// Associate a unique number to each 2D position
uint8_t linear_position(Vector2D position) {
    return position.x + position.y * MAP_SIZE;
}

// The distance heuristic to decide where to go next.
uint8_t heuristic_distance(Vector2D start, Vector2D end) {
    //**** TODO ***************
}

// Implement the A* algorithm
void find_path(Vector2D start, Vector2D end, uint8_t map[MAP_SIZE][MAP_SIZE]) {
    // init nodes: each map position is associated with a node
    Node nodes[MAP_SIZE * MAP_SIZE];
    for(int row = 0; row < MAP_SIZE; row++) {
        for(int col = 0; col < MAP_SIZE; col++) {
            int linpos = linear_position((Vector2D){row, col});
            nodes[linpos].position = (Vector2D){row, col};
            nodes[linpos].gCost = 0xFF;
            nodes[linpos].hCost = 0xFF;
            nodes[linpos].parent = NULL;
        }
    }

    // init open list: list of nodes left to explores
    SortedList openList;
    SortedList_init(&openList);

    // set start node
    Node* startNode = &nodes[linear_position(start)];
    startNode->gCost = 0;
    startNode->hCost = heuristic_distance(start, end);

    // add start node to open list
    SortedList_insert(&openList, startNode);

    // while open list is not empty
    while (!SortedList_isempty(&openList)) {
        Node* currentNode = SortedList_pop(&openList);

        // if current node is end node
        if (currentNode->hCost == 0) {
            // !!!!!!!!!! Reconstruct path !!!!!!!!!!

            // for each node in path
            Node* pathNode = currentNode;
            while (pathNode != NULL) {
                // set map
                map[pathNode->position.x][pathNode->position.y] = MAP_PATH;

                // set next node
                pathNode = pathNode->parent;
            }

            map[start.x][start.y] = MAP_START;
            map[end.x][end.y] = MAP_END;
            
            return; //EXIT heer
        }

        // here current node is not end node
        // for each neighbour
        for(int i = 0; i < 4; i++) {
            Vector2D neighbourPosition = currentNode->position;
            switch (i) {
                case 0:
                    neighbourPosition.x++;
                    break;
                case 1:
                    neighbourPosition.x--;
                    break;
                case 2:
                    neighbourPosition.y++;
                    break;
                case 3:
                    neighbourPosition.y--;
                    break;
            }

            // ************ TODO ***************************
        }
    }
}


int main(){
    mmu_init();

    uint8_t map[MAP_SIZE][MAP_SIZE];
    map_init(map);

    display_map(map);

    u32 start_time = read_u32(CLINT_TIME);
    find_path((Vector2D){0, 0}, (Vector2D){7, 7}, map);
    u32 end_time = read_u32(CLINT_TIME);
    u64 delta_time = end_time - start_time;
    
    display_map(map);

    sim_puts("Start time: ");
    sim_putudec(start_time);
    sim_puts("\n");

    sim_puts("End time: ");
    sim_putudec(end_time);
    sim_puts("\n");

    sim_puts("Duration: ");
    sim_putudec(delta_time);
    sim_puts("\n");

    sim_puts("*** main exit ***\n");
    pass();
    return 0;
}

