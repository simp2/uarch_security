#!/usr/bin/env nix-shell
#!nix-shell ../env/shell.nix -i bash --pure

eval "$commands"
cd "$SCRIPTPATH"

make clean
echo "Cleaned"
make MARCH=rv32imafd MABI=ilp32