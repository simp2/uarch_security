# L1 Algorithme de recherche de chemin

Cette leçon ne se préoccupe pas de sécurité.
Il s'agit ici d'explorer le fonctionnement de plusieurs microarchitectures : une à exécution dans l'ordre, l'autre à exécution dans le désordre.

## Recherche de chemin

Nous cherchons à exécuter un algorithme très simple de recherche de chemin.
Pour un point de départ en 2D à coordonnées entières, un point d'arrivée, et une carte (*map*) décrivant les cases autorisées et interdites, nous cherchons à établir le plus court chemin reliant ces points.

## Énoncé

### Compléter l'algorithme de recherche de chemin

Certaines parties de l'algorithme doivent être complétées :
- Q1) Définir un calcul heuristique de distance dans *heuristic_distance*
- Q2) Compléter la partie manquante de la fonction *find_path* de recherche de chemin.

L'objectif ici est de vous familiariser avec l'algorithme et de comprendre son fonctionnement.

Pour debugger votre algorithme et visualiser le résultat, exécuter votre programme sur le Nax (out-of-order).

Compiler votre programme pour obtenir un binaire *.elf*.
```bash
./make.outoforder.sh
```

Puis exécuter avec
```bash
./execute.sh
```

Ce qui affichera la carte et le chemin choisit par votre algorithme.
Q3) Joignez la carte avec le chemin sélectionner par vorte algorithme dans votre rapport.


### Simuler la recherche de chemin sur qtrvsim (in-order)

Recompiler maintenant pour cibler le cœur in-order (32-bit) simuler par *qtrvsim*.

```bash
./make.inorder.sh
```

Lancer l'interface graphique de *qtrvsim* avec

```bash
./view.inorder.sh
```

Une fenêtre s'ouvre et demande la configuration de votre cœur.
Choisissez:
- Le preset "Pipelined with hazard unit and cache".
- Elf executable: le fichier .elf que vous venez de générer dans le dossier *build*.

Puis cliquez sur "Load machine". 

Le processeur ne s'arrête jamais sans une indication de où il faut s'arrêter.
Placez des breakpoints au niveau de la fin du programme.

Q4) Documenter comment vous déterminer où placer ce breakpoint.

Regarder l'exécution en détail à partir du début de la fonction *find_path*.

Identifier au cours de cette exécution:
- Q5) Un cache miss L1I. Expliquez en détail ce qui s'est passé. Expliquer comment le tag du cache est calculé.
- Q6) Un cache hit et un cache miss L1D.

Q7) Observer l'exécution d'un saut direct *jal* ou *j*.
Décrivez le comportement du pipeline lors de l'exécution de cette instruction.


Q8) Combien de cycles d'horloge sont-ils nécessaire au total pour exécuter la fonction *find_path*.



### Simuler la recherche de chemin sur Nax (out-of-order)

Reompiler votre programme pour obtenir un binaire *.elf* pour le Nax (64-bit).

```bash
./make.outoforder.sh
```

Simulez votre algorithme et générez la trace Gem5.

```bash
./execute.outoforder.sh
```

Vous pouvez maintenant visualiser la trace avec

```bash
./view.outoforder.sh
```

Attention, si vous avez une erreur sous Ubuntu 24.04 ou plus récent, utilisez le fix correspondant.

```bash
../ubuntu_fix.sh
```

Essayez de comprendre le fonctionnement de cette visualisation.
Les couleurs dans cette traces ne correspondent pas aux couleurs dans le cas in-order.

*À expliquer dans votre compte rendu*.
N'hésitez pas à en discuter avec vos encadrants.

En particulier, répondez (avec texte et/ou captures d'écran) aux points suivants :

- À quoi correspond la pente des points lorsque l'on dézoome sur Konata ? Que signifie une pente plus ou moins raide ?
- Identifier les motifs suivants dans Konata :
    - Un L1I miss
    - Un L1D miss
    - Une mauvaise prédiction de branchement
    - Une bonne prédiction de branchement
    - Comment fonctionne le prédicteur de branchement du Nax.
    - Des sauts indirects ? Quelles instructions sont exécutées après ces sauts ? Pourquoi ?
    - Trouver une boucle, et identifier les instructions qui la composent.      
        * Est-ce que la prédiction de branchement sur la condition de sortie de boucle est correcte tout de suite ? 
        * Sinon, quand est-ce qu'elle devient correcte ? Pourquoi ?

### Modifier le cœur

Vous pouvez modifier la configuration du cœur Nax dans le fichier **nax/src/main/scala/Gen.scala** en modifiant l'objet *Config* ou l'objet *Gen64*.

Essayez de comprendre comment fonctionne cette configuration. N'hésitez pas à en discuter avec vos encadrants.

- Modifier les paramètres du cœur pour améliorer les performances du calcul. C'est-à-dire minimiser la durée d'exécution.
Quels paramètres avez-vous modifiés, pourquoi, et quels sont les gains obtenus ?
- Si on augmente la taille du GHR (global history register), quel est l'impact sur les performances ? Pourquoi ?