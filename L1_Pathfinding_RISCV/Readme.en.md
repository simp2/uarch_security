
# L1 Pathfinding Algorithm

This lesson is not concerned with security.
It is about getting to grips with the Nax processor and understanding how it works.

## Pathfinding

We aim to execute a very simple pathfinding algorithm.
For a starting point in 2D with integer coordinates, an endpoint, and a map describing allowed and forbidden squares, we seek to establish the shortest path connecting these points.

## Objective

### Complete the Pathfinding Algorithm

Some parts of the algorithm need to be completed:
- Define a heuristic distance calculation in *heuristic_distance*
- Complete the missing part of the *find_path* function for pathfinding.

The goal here is to familiarize yourself with the algorithm and understand how it works.

### Simulate the Pathfinding

Simulate your algorithm and generate the Gem5 trace.

```bash
./execute.sh
```

You can now view the trace with

```bash
./view_trace.sh
```

Try to understand how this visualization works.
*To be explained in your report*.
Feel free to discuss it with your supervisors.

In particular, answer (with text and/or screenshots) the following points:

- What does the slope of the points mean when zooming out on Konata? What does a steeper or less steep slope signify?
- Identify the following patterns in Konata:
    - An L1I miss
    - An L1D miss
    - A bad branch prediction
    - A good branch prediction
    - How does the Nax's branch predictor work.
    - Indirect jumps? What instructions are executed after these jumps? Why?
    - Find a loop, and identify the instructions that compose it.      
        * Is the branch prediction on the loop exit condition correct right away? 
        * If not, when does it become correct? Why?

### Modify the Core

You can modify the Nax core configuration in the file **nax/src/main/scala/Gen.scala** by changing the *Config* object or the *Gen64* object.

Try to understand how this configuration works. Feel free to discuss it with your supervisors.

- Modify the core parameters to improve the calculation performance. That is to minimize the execution time.
What parameters did you modify, why, and what gains were obtained?
- If we increase the size of the GHR (global history register), what is the impact on performance? Why?
